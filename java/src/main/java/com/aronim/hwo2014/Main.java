package com.aronim.hwo2014;

import com.aronim.hwo2014.message.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.Socket;

import static com.aronim.hwo2014.message.OutputMessages.*;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    private final Gson gson;

    private final Socket socket;

    private final PrintWriter writer;

    private final BufferedReader reader;

    private final GameState gameState;

    public Main(String host, int port) throws IOException {

        socket = new Socket(host, port);

        writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        gson = new GsonBuilder()
                .registerTypeAdapter(InputMessage.class, new InputMessageDeserializer())
                .create();

        gameState = new GameState();
    }

    Main join(String botName, String botKey) {

        send(joinMessage(botName, botKey));

        return this;
    }

    Main race() throws IOException {

        String line;

        while ((line = reader.readLine()) != null) {

            LOGGER.debug("IN  - " + line);

            InputMessage inputMessage = gson.fromJson(line, InputMessage.class);

            handleInputMessage(inputMessage);
        }

        return this;
    }

    private void handleInputMessage(InputMessage inputMessage) {

        if (inputMessage != null && inputMessage.getMessageType() != null) {

            switch (inputMessage.getMessageType()) {
                case JOIN:
                    handleJoinedMessage((JoinedMessage) inputMessage);
                    break;
                case YOURCAR:
                    handleYourCarMessage((YourCarMessage) inputMessage);
                    break;
                case CARPOSITIONS:
                    handleCarPositionsMessage((CarPositionsMessage) inputMessage);
                    break;
                case GAMEINIT:
                    handleGameInitialisedMessage((GameInitialisedMessage) inputMessage);
                    break;
                case GAMEEND:
                    handleGameEndedMessage((GameEndedMessage) inputMessage);
                    break;
                case GAMESTART:
                    handleGameStartedMessage((GameStartedMessage) inputMessage);
                    break;
                case CRASH:
                    handleCrashMessage((CrashMessage) inputMessage);
                    break;
                default:
                    send(pingMessage());
                    break;
            }
        }
    }

    private void handleCrashMessage(CrashMessage crashMessage) {
        System.out.println("Crashed");
    }

    private void handleGameStartedMessage(GameStartedMessage gameStartedMessage) {
        System.out.println("Race start");
    }

    private void handleGameEndedMessage(GameEndedMessage gameEndedMessage) {
        System.out.println("Race end");
    }

    private void handleGameInitialisedMessage(GameInitialisedMessage gameInitialisedMessage) {
        gameState.setRace(gameInitialisedMessage.getRace());
    }

    private void handleYourCarMessage(YourCarMessage yourCarMessage) {
        gameState.setMyCarIdentifier(yourCarMessage.getId());
    }

    private void handleJoinedMessage(JoinedMessage joinedMessage) {
        System.out.println("Joined");
    }

    private void handleCarPositionsMessage(CarPositionsMessage carPositionsMessage) {

        int gameTick = carPositionsMessage.getGameTick();

        for (CarPosition carPosition : carPositionsMessage.getCarPositions()) {
            gameState.newCarPosition(gameTick, carPosition);
        }

        if ((gameTick % 2) == 0) {

            SwitchDecision switchDecision = gameState.getSwitchDecision();
            switch (switchDecision) {

                case LEFT:
                    send(switchLeftMessage(gameTick));
                    return;
                case RIGHT:
                    send(switchRightMessage(gameTick));
                    return;
            }
        }

        if (gameState.shouldAccelerate()) {
            send(throttleMessage(gameTick, 1.0d));
        } else {
            send(throttleMessage(gameTick, 0.0d));
        }
    }

    private void send(String message) {

        LOGGER.debug("OUT - " + message);

        writer.println(message);
        writer.flush();
    }

    public static void main(String... args) throws IOException {

        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        new Main(host, port)
                .join(botName, botKey)
                .race();
    }
}