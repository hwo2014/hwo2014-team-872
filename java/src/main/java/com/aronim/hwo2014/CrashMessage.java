package com.aronim.hwo2014;

import com.aronim.hwo2014.message.InputMessage;
import com.aronim.hwo2014.message.MessageType;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-16
 * Time: 19h39
 */
public final class CrashMessage implements InputMessage {

    @Override
    public MessageType getMessageType() {
        return MessageType.CRASH;
    }
}
