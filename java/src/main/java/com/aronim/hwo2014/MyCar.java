package com.aronim.hwo2014;

import com.aronim.hwo2014.message.CarIdentifier;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-15
 * Time: 18h17
 */
public class MyCar {

    private CarIdentifier id;

    private MyCar(CarIdentifier id) {
        this.id = id;
    }

    public CarIdentifier getId() {
        return id;
    }
}
