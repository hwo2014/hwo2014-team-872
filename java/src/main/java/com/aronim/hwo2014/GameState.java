package com.aronim.hwo2014;

import com.aronim.hwo2014.message.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-15
 * Time: 18h28
 */
public final class GameState implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(GameState.class);

    private CarIdentifier myCarIdentifier;

    private Race race;

    private Map<CarIdentifier, Double> previousDistanceByCarId;

    private Map<CarIdentifier, Double> currentVelocityByCarId;

    private Map<CarIdentifier, CarPosition> currentPositionByCarId;

    public GameState() {

        this.previousDistanceByCarId = new HashMap<>();

        this.currentVelocityByCarId = new HashMap<>();

        this.currentPositionByCarId = new HashMap<>();
    }

    public void setMyCarIdentifier(CarIdentifier myCarIdentifier) {
        this.myCarIdentifier = myCarIdentifier;
    }

    public void setRace(Race race) {
        this.race = race;
    }

    public void newCarPosition(Integer gameTick, CarPosition carPosition) {

        this.race.initialise();

        CarIdentifier carId = carPosition.getId();

        Double previousDistance = this.previousDistanceByCarId.get(carId);
        if (previousDistance == null) {
            previousDistance = 0.0d;
        }

        double currentDistance = this.race.getLapDistance(carPosition);

        this.previousDistanceByCarId.put(carId, currentDistance);

        double velocity = currentDistance - previousDistance;

        this.currentVelocityByCarId.put(carId, velocity);

        this.currentPositionByCarId.put(carId, carPosition);
    }

    public Boolean shouldAccelerate() {

        Double currentVelocity = getMyCurrentVelocity();

        CarPosition carPosition = getMyCurrentPosition();

        if (race.isOnCurvedPiece(carPosition)) {
            return currentVelocity < 5.75;
        }

        Double distanceToNextCurvedPiece = race.getDistanceToNextCurvedPiece(carPosition);

        Double deceleration = -0.1;

        Double finalVelocity = 5.75;

        Double nextCornersAngle = Math.abs(race.getNextCornersAngle(carPosition));

        if (nextCornersAngle <= 45) {
            finalVelocity = 9.75;
        }

        if (nextCornersAngle > 45 && nextCornersAngle <= 90) {
            finalVelocity = 7.75;
        }

        if (nextCornersAngle > 90 && nextCornersAngle <= 180) {
            finalVelocity = 5.75;
        }

        if (finalVelocity > currentVelocity) {
            return true;
        }

        double changeInVelocity = finalVelocity - currentVelocity;

        Double timeRequiredToSlowDown = (changeInVelocity / deceleration);

        Double distanceRequiredToSlowDown = timeRequiredToSlowDown * Math.abs(changeInVelocity) / 2;

        return distanceRequiredToSlowDown < distanceToNextCurvedPiece;
    }

    public SwitchDecision getSwitchDecision() {

        CarPosition carPosition = getMyCurrentPosition();

        int currentLane = carPosition.getCurrentLaneIndex();

        int preferredLane = race.getPreferredLaneIndex(carPosition);

        return race.getSwitchDecision(currentLane, preferredLane);

    }

    public Double getMyCurrentVelocity() {
        return this.currentVelocityByCarId.get(myCarIdentifier);
    }

    public CarPosition getMyCurrentPosition() {
        return this.currentPositionByCarId.get(myCarIdentifier);
    }
}
