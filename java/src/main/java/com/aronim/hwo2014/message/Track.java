package com.aronim.hwo2014.message;

import java.io.Serializable;
import java.util.List;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-15
 * Time: 17h47
 */
public final class Track implements Serializable {

    String id;

    String name;

    List<Piece> pieces;

    List<Lane> lanes;

    StartingPoint startingPoint;

    public Track() {
    }

    public void initialise() {

        for (Piece piece : pieces) {
            piece.initialise(lanes);
        }

        initialiseAccumulativeDistance();

        initialiseDistanceToNextCurvedPiece();

        initialiseTotalAngleOfNextCurvedPiece();
    }

    private void initialiseAccumulativeDistance() {

        for (Lane lane : lanes) {

            Integer laneIndex = lane.index;

            Double accumulativeLaneDistance = 0.0;

            for (Piece piece : pieces) {

                piece.setAccumulativeLaneDistanceAtBeginningOfPiece(laneIndex, accumulativeLaneDistance);

                accumulativeLaneDistance += piece.getLaneDistance(laneIndex);
            }
        }
    }

    private void initialiseDistanceToNextCurvedPiece() {

        int numberOfPieces = pieces.size();
        for (int currentPieceIndex = 0; currentPieceIndex < numberOfPieces; currentPieceIndex++) {

            Piece currentPiece = pieces.get(currentPieceIndex);

            if (currentPiece.isStraightPiece()) {
                currentPiece.distanceToNextCurvedPiece = getDistanceToNextCurvedPiece(numberOfPieces, currentPieceIndex);
            } else {
                Piece nextPiece = pieces.get((currentPieceIndex + 1) % numberOfPieces);
                if (nextPiece.isStraightPiece()) {
                    currentPiece.setNextPieceIsStraight(true);
                    currentPiece.distanceToNextCurvedPiece = getDistanceToNextCurvedPiece(numberOfPieces, currentPieceIndex);
                }
            }
        }
    }

    private void initialiseTotalAngleOfNextCurvedPiece() {

        int numberOfPieces = pieces.size();
        for (int currentPieceIndex = 0; currentPieceIndex < numberOfPieces; currentPieceIndex++) {

            Piece currentPiece = pieces.get(currentPieceIndex);

            currentPiece.nextCornersAngle = getTotalAngleOfNextCurvedPiece(numberOfPieces, currentPieceIndex, currentPiece.angle);
        }
    }

    private Double getDistanceToNextCurvedPiece(int numberOfPieces, int currentPieceIndex) {

        Double distanceToNextCurvedPiece = 0.0;

        for (int i = currentPieceIndex + 1; i < numberOfPieces * 2; i++) {

            Piece nextPiece = pieces.get(i % numberOfPieces);

            if (nextPiece.isStraightPiece()) {
                distanceToNextCurvedPiece += nextPiece.length;
            } else {
                break;
            }
        }

        return distanceToNextCurvedPiece;
    }

    private Double getTotalAngleOfNextCurvedPiece(int numberOfPieces, int currentPieceIndex, Double currentPieceAngle) {

        Double totalAngleOfNextCurvedPiece = 0.0;

        int nextCornerStartIndex = findNextCornerStartndex(numberOfPieces, currentPieceIndex, currentPieceAngle);
        int nextCornerEndIndex = findCornerEndIndex(numberOfPieces, nextCornerStartIndex);

        for (int i = nextCornerStartIndex; i <= nextCornerEndIndex; i++) {

            Piece piece = pieces.get(i % numberOfPieces);

            totalAngleOfNextCurvedPiece += piece.angle;
        }

        return totalAngleOfNextCurvedPiece;
    }

    private int findCornerEndIndex(int numberOfPieces, int cornerStartIndex) {

        Double initialTurningAngle = pieces.get(cornerStartIndex % numberOfPieces).angle;

        for (int i = cornerStartIndex + 1; i < numberOfPieces * 2; i++) {

            Piece piece = pieces.get(i % numberOfPieces);

            if (piece.isStraightPiece()) {
                return i - 1;
            }

            if (piece.angle / initialTurningAngle < 0) {
                return i - 1;
            }
        }

        throw new RuntimeException("Should never get here");
    }

    private int findNextCornerStartndex(int numberOfPieces, int currentPieceIndex, Double currentPieceAngle) {

        Boolean startedInACorner = currentPieceAngle != null && currentPieceAngle != 0.0;

        Boolean exitedStartingCorner = false;

        for (int i = currentPieceIndex + 1; i < numberOfPieces * 2; i++) {

            Piece piece = pieces.get(i % numberOfPieces);

            if (piece.isStraightPiece()) {
                if (startedInACorner) {
                    exitedStartingCorner = true;
                }

                continue;
            }

            Boolean turningInSameDirectionAsStartingCorner = false;
            if (startedInACorner) {
                turningInSameDirectionAsStartingCorner = currentPieceAngle / piece.angle > 0;
            }

            if (piece.isCurvedPiece() && turningInSameDirectionAsStartingCorner && !exitedStartingCorner) {
                continue;
            }

            return i;
        }

        throw new RuntimeException("Should never get here");
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Piece> getPieces() {
        return pieces;
    }

    public List<Lane> getLanes() {
        return lanes;
    }

    public StartingPoint getStartingPoint() {
        return startingPoint;
    }

    public Double getLapDistance(CarPosition carPosition) {

        PiecePosition piecePosition = carPosition.getPiecePosition();

        Piece currentPiece = pieces.get(piecePosition.getPieceIndex());

        return currentPiece.getLapDistance(piecePosition);
    }

    public Double getDistanceToNextCurvedPiece(CarPosition carPosition) {

        PiecePosition piecePosition = carPosition.getPiecePosition();

        Piece currentPiece = pieces.get(piecePosition.getPieceIndex());

        return currentPiece.getDistanceToNextCurvedPiece(piecePosition);
    }

    public Double getNextCornersAngle(CarPosition carPosition) {

        PiecePosition piecePosition = carPosition.getPiecePosition();

        Piece currentPiece = pieces.get(piecePosition.getPieceIndex());

        return currentPiece.nextCornersAngle;
    }

    public Double getLaneDistanceFromCenter(CarPosition carPosition) {
        PiecePosition piecePosition = carPosition.getPiecePosition();
        LaneChange laneChange = piecePosition.getLane();
        int currentLaneIndex = laneChange.getEndLaneIndex();

        return lanes.get(currentLaneIndex).distanceFromCenter;
    }

    public SwitchDecision getSwitchDecision(int currentLaneIndex, int preferredLaneIndex) {

        Lane currentLane = lanes.get(currentLaneIndex);
        Lane preferredLane = lanes.get(preferredLaneIndex);

        Double currentLaneDistanceFromCenter = currentLane.distanceFromCenter;
        Double preferredLaneDistanceFromCenter = preferredLane.distanceFromCenter;

        if (currentLaneDistanceFromCenter < preferredLaneDistanceFromCenter) {
            return SwitchDecision.RIGHT;
        }

        if (currentLaneDistanceFromCenter > preferredLaneDistanceFromCenter) {
            return SwitchDecision.LEFT;
        }

        return SwitchDecision.FORWARD;
    }

    public int getPreferredLaneIndex(CarPosition carPosition) {

        PiecePosition piecePosition = carPosition.getPiecePosition();
        Piece currentPiece = pieces.get(piecePosition.getPieceIndex());

        Double nextCornersAngle = currentPiece.nextCornersAngle;

        if (nextCornersAngle > 0) {
            return findRightMostLaneIndex();
        } else {
            return findLeftMostLaneIndex();
        }
    }

    public boolean isOnCurvedPiece(CarPosition carPosition) {

        PiecePosition piecePosition = carPosition.getPiecePosition();
        Piece currentPiece = pieces.get(piecePosition.getPieceIndex());

        return currentPiece.isCurvedPiece();
    }

    private int findLeftMostLaneIndex() {

        int leftMostLaneIndex = 0;

        Double leftMostLaneDistanceFromCenter = 0.0;

        for (int i = 0; i < lanes.size(); i++) {

            Lane lane = lanes.get(i);
            if (lane.distanceFromCenter < leftMostLaneDistanceFromCenter) {
                leftMostLaneIndex = i;
                leftMostLaneDistanceFromCenter = lane.distanceFromCenter;
            }
        }

        return leftMostLaneIndex;
    }

    private int findRightMostLaneIndex() {

        int rightMostLaneIndex = 0;

        Double rightMostLaneDistanceFromCenter = 0.0;

        for (int i = 0; i < lanes.size(); i++) {

            Lane lane = lanes.get(i);
            if (lane.distanceFromCenter > rightMostLaneDistanceFromCenter) {
                rightMostLaneIndex = i;
                rightMostLaneDistanceFromCenter = lane.distanceFromCenter;
            }
        }

        return rightMostLaneIndex;
    }
}
