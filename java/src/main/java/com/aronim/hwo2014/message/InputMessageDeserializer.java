package com.aronim.hwo2014.message;

import com.aronim.hwo2014.CrashMessage;
import com.google.gson.*;

import java.lang.reflect.Type;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-15
 * Time: 17h02
 */
public class InputMessageDeserializer implements JsonDeserializer<InputMessage> {

    @Override
    public InputMessage deserialize(JsonElement jsonElement, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject jsonObject = jsonElement.getAsJsonObject();
        String msgType = jsonObject.get("msgType").getAsString();

        MessageType messageType = MessageType.getMessageType(msgType);
        if (messageType == null) {
            return null;
        }

        switch (messageType) {
            case YOURCAR:
                return deserialiseYourCarMessage(jsonObject);
            case JOIN:
                return deserialiseJoinedMessage();
            case CARPOSITIONS:
                return deserialiseCarPositionsMessage(jsonElement, context);
            case GAMEINIT:
                return deserialiseGameInitialisedMessage(context, jsonObject);
            case GAMEEND:
                return deserialiseGameEndedMessage();
            case GAMESTART:
                return deserialiseGameStartedMessage();
            case CRASH:
                return deserialiseCrashMessage();
            default:
                break;
        }

        return null;
    }

    private InputMessage deserialiseYourCarMessage(JsonObject jsonObject) {

        JsonObject data = jsonObject.get("data").getAsJsonObject();
        String name = data.get("name").getAsString();
        String color = data.get("color").getAsString();

        return new YourCarMessage(new CarIdentifier(name, color));
    }

    private JoinedMessage deserialiseJoinedMessage() {

        return new JoinedMessage();
    }

    private InputMessage deserialiseCarPositionsMessage(JsonElement jsonElement, JsonDeserializationContext context) {

        return context.deserialize(jsonElement, CarPositionsMessage.class);
    }

    private InputMessage deserialiseGameInitialisedMessage(JsonDeserializationContext context, JsonObject jsonObject) {

        JsonObject data = jsonObject.getAsJsonObject("data");
        return context.deserialize(data, GameInitialisedMessage.class);
    }

    private GameEndedMessage deserialiseGameEndedMessage() {

        return new GameEndedMessage();
    }

    private GameStartedMessage deserialiseGameStartedMessage() {

        return new GameStartedMessage();
    }

    private InputMessage deserialiseCrashMessage() {
        return new CrashMessage();
    }
}
