package com.aronim.hwo2014.message;

import java.io.Serializable;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-15
 * Time: 17h47
 */
public final class Lane implements Serializable {

    /**
     * A positive value tells that the lanes is to the right from the center line
     * while a negative value indicates a lane to the left from the center.
     */
    Double distanceFromCenter;

    int index;

    public Lane() {
    }

    public int getIndex() {
        return index;
    }
}
