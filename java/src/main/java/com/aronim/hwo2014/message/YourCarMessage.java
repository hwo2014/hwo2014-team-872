package com.aronim.hwo2014.message;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-15
 * Time: 17h58
 */
public final class YourCarMessage implements InputMessage {

    private CarIdentifier id;

    public YourCarMessage(CarIdentifier id) {
        this.id = id;
    }

    public CarIdentifier getId() {
        return id;
    }

    @Override
    public MessageType getMessageType() {
        return MessageType.YOURCAR;
    }
}
