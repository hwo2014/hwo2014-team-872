package com.aronim.hwo2014.message;

import java.io.Serializable;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-15
 * Time: 17h21
 */
public final class CarPosition implements Serializable {

    private CarIdentifier id;

    private double angle;

    private PiecePosition piecePosition;

    public CarPosition() {
    }

    public CarIdentifier getId() {
        return id;
    }

    public double getAngle() {
        return angle;
    }

    public PiecePosition getPiecePosition() {
        return piecePosition;
    }

    public int getCurrentLaneIndex() {
        LaneChange laneChange = piecePosition.getLane();
        return laneChange.getEndLaneIndex();
    }
}
