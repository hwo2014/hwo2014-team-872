package com.aronim.hwo2014.message;

import java.io.Serializable;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-15
 * Time: 17h50
 */
public final class Position implements Serializable {

    private double x;

    private double y;

    public Position() {
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
