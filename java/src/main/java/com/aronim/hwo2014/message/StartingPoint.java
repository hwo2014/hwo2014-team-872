package com.aronim.hwo2014.message;

import java.io.Serializable;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-15
 * Time: 17h47
 */
public final class StartingPoint implements Serializable {

    private Position position;

    private double angle;

    public StartingPoint() {
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }
}
