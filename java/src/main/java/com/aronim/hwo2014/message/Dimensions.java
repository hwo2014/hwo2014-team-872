package com.aronim.hwo2014.message;

import java.io.Serializable;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-15
 * Time: 17h48
 */
public final class Dimensions implements Serializable {

    private double length;

    private double width;

    private double guideFlagPosition;

    public Dimensions() {
    }

    public double getLength() {
        return length;
    }

    public double getWidth() {
        return width;
    }

    public double getGuideFlagPosition() {
        return guideFlagPosition;
    }
}
