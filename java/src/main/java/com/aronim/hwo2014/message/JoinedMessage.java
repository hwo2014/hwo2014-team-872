package com.aronim.hwo2014.message;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-15
 * Time: 17h24
 */
public class JoinedMessage implements InputMessage {

    @Override
    public MessageType getMessageType() {
        return MessageType.JOIN;
    }
}
