package com.aronim.hwo2014.message;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-15
 * Time: 17h06
 */
public enum MessageType {

    YOURCAR("yourCar"),
    JOIN("join"),
    CARPOSITIONS("carPositions"),
    GAMEINIT("gameInit"),
    GAMESTART("gameStart"),
    GAMEEND("gameEnd"),
    CRASH("crash"),
    SPAWN("spawn"),
    TOURNAMENTEND("tournamentEnd"),
    ;

    public final String name;

    MessageType(String name) {

        this.name = name;
    }

    public static MessageType getMessageType(String name) {

        for (MessageType messageType : values()) {
            if (messageType.name.equals(name)) {
                return messageType;
            }
        }

        return null;
    }
}
