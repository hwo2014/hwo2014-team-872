package com.aronim.hwo2014.message;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-15
 * Time: 17h53
 */
public final class Piece implements Serializable {

    Double length;

    @SerializedName("switch")
    boolean hasSwitch = false;

    Double angle;

    Double radius;

    Map<Integer, Double> laneArcLengthByIndex;

    // Accumulative lane distance at beginning of piece
    Map<Integer, Double> accumulativeLaneDistanceByIndex;

    Double distanceToNextCurvedPiece;

    boolean nextPieceIsStraight = false;

    Double nextCornersAngle;

    public Piece() {
    }

    public void initialise(List<Lane> lanes) {

        this.laneArcLengthByIndex = new HashMap<>();

        if (isCurvedPiece()) {
            for (Lane lane : lanes) {
                Double arcLength = calculateArcLength(angle, radius, lane.distanceFromCenter);
                this.laneArcLengthByIndex.put(lane.index, arcLength);
            }
        }
    }

    public boolean isStraightPiece() {
        return angle == null || (angle < 0.0001 && angle > -0.0001);
    }

    public boolean isCurvedPiece() {
        return !isStraightPiece();
    }

    public boolean isRightTurn() {
        return angle != null && angle > 0.0;
    }

    public boolean isLeftTurn() {
        return angle != null && angle < 0.0;
    }

    public Double getLaneDistance(Integer laneIndex) {

        if (isStraightPiece()) {
            return length;
        } else {
            return laneArcLengthByIndex.get(laneIndex);
        }
    }

    private Double calculateArcLength(Double angle, Double centerRadius, Double distanceFromCenter) {

        Double laneRadius = getLaneRadius(centerRadius, distanceFromCenter);

        return calculateArcLength(angle, laneRadius);
    }

    private Double getLaneRadius(Double centerRadius, Double distanceFromCenter) {

        if (isRightTurn()) {
            return centerRadius - distanceFromCenter;
        } else {
            return centerRadius + distanceFromCenter;
        }
    }

    private static Double calculateArcLength(Double angle, Double radius) {

        Double circumference = 2 * Math.PI * radius;

        return (Math.abs(angle) / 360) * circumference;
    }

    public void setAccumulativeLaneDistanceAtBeginningOfPiece(Integer laneIndex, Double accumulativeLaneDistance) {

        if (this.accumulativeLaneDistanceByIndex == null) {
            this.accumulativeLaneDistanceByIndex = new HashMap<>();
        }

        this.accumulativeLaneDistanceByIndex.put(laneIndex, accumulativeLaneDistance);
    }

    public Double getAccumulativeLaneDistanceAtBeginningOfPiece(Integer laneIndex) {
        return this.accumulativeLaneDistanceByIndex.get(laneIndex);
    }

    public Double getLapDistance(PiecePosition piecePosition) {

        int startLaneIndex = piecePosition.getLane().getStartLaneIndex();
        Double laneDistance = getAccumulativeLaneDistanceAtBeginningOfPiece(startLaneIndex);

        return piecePosition.getInPieceDistance() + laneDistance;
    }

    public Double getDistanceToNextCurvedPiece(PiecePosition piecePosition) {

        if (isCurvedPiece() && !nextPieceIsStraight) {
            return 0.0d;
        }

        int endLaneIndex = piecePosition.getLane().getEndLaneIndex();
        Double distanceLeftInPiece = getLaneDistance(endLaneIndex) - piecePosition.getInPieceDistance();

        return distanceLeftInPiece + distanceToNextCurvedPiece;
    }

    public void setNextPieceIsStraight(boolean nextPieceIsStraight) {
        this.nextPieceIsStraight = nextPieceIsStraight;
    }

}
