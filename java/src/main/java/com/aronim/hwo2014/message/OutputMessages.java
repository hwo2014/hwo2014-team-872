package com.aronim.hwo2014.message;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-15
 * Time: 16h46
 */
public class OutputMessages {

    public static String joinMessage(String botName, String botKey) {
        return "{\"msgType\":\"join\",\"data\": {\"name\":\"" + botName + "\",\"key\":\"" + botKey + "\"}}";
    }

    public static String pingMessage() {
        return "{\"msgType\":\"ping\"}";
    }

    public static String pingMessage(int gameTick) {
        return "{\"msgType\":\"ping\",\"gameTick\":" + gameTick + "}";
    }

    public static String throttleMessage(int gameTick, double value) {
        return "{\"msgType\":\"throttle\",\"data\":" + value + ",\"gameTick\":" + gameTick + "}";
    }

    public static String switchLeftMessage(int gameTick) {
        return "{\"msgType\":\"switchLane\",\"data\":\"Left\",\"gameTick\":" + gameTick + "}";
    }

    public static String switchRightMessage(int gameTick) {
        return "{\"msgType\":\"switchLane\",\"data\":\"Right\",\"gameTick\":" + gameTick + "}";
    }

}
