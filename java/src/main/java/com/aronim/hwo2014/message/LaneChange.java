package com.aronim.hwo2014.message;

import java.io.Serializable;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-15
 * Time: 17h37
 */
public final class LaneChange implements Serializable {

    private int startLaneIndex;

    private int endLaneIndex;

    public LaneChange() {
    }

    public int getStartLaneIndex() {
        return startLaneIndex;
    }

    public int getEndLaneIndex() {
        return endLaneIndex;
    }
}
