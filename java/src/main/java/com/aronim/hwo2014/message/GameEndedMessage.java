package com.aronim.hwo2014.message;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-15
 * Time: 17h27
 */
public class GameEndedMessage implements InputMessage {
    @Override
    public MessageType getMessageType() {
        return MessageType.GAMEEND;
    }
}
