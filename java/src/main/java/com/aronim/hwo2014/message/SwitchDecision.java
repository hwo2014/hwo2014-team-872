package com.aronim.hwo2014.message;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-17
 * Time: 14h12
 */
public enum SwitchDecision {
    LEFT,
    FORWARD,
    RIGHT
}
