package com.aronim.hwo2014.message;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-15
 * Time: 17h03
 */
public interface InputMessage {

    MessageType getMessageType();
}
