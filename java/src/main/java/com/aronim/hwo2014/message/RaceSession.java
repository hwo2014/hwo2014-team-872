package com.aronim.hwo2014.message;

import java.io.Serializable;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-15
 * Time: 17h55
 */
public final class RaceSession implements Serializable {

    private int laps;

    private int maxLapTimeMs;

    private boolean quickRace;

    public RaceSession() {
    }

    public int getLaps() {
        return laps;
    }

    public int getMaxLapTimeMs() {
        return maxLapTimeMs;
    }

    public boolean isQuickRace() {
        return quickRace;
    }
}
