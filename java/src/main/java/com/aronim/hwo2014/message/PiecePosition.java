package com.aronim.hwo2014.message;

import java.io.Serializable;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-15
 * Time: 17h36
 */
public final class PiecePosition implements Serializable {

    private int pieceIndex;

    private double inPieceDistance;

    private LaneChange lane;

    private int lap;

    public PiecePosition() {
    }

    public int getPieceIndex() {
        return pieceIndex;
    }

    public double getInPieceDistance() {
        return inPieceDistance;
    }

    public LaneChange getLane() {
        return lane;
    }

    public int getLap() {
        return lap;
    }
}
