package com.aronim.hwo2014.message;

import java.io.Serializable;
import java.util.List;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-15
 * Time: 17h43
 */
public final class Race implements Serializable {

    Boolean initialised = false;

    Track track;

    List<Car> cars;

    RaceSession raceSession;

    public Race() {
    }

    public void initialise() {
        if (!initialised) {
            this.track.initialise();
        }
    }

    public Track getTrack() {
        return track;
    }

    public List<Car> getCars() {
        return cars;
    }

    public Double getLapDistance(CarPosition carPosition) {
        return track.getLapDistance(carPosition);
    }

    public Double getDistanceToNextCurvedPiece(CarPosition carPosition) {
        return track.getDistanceToNextCurvedPiece(carPosition);
    }

    public Double getNextCornersAngle(CarPosition carPosition) {
        return track.getNextCornersAngle(carPosition);
    }

    public Double getLaneDistanceFromCenter(CarPosition carPosition) {
        return track.getLaneDistanceFromCenter(carPosition);
    }

    public SwitchDecision getSwitchDecision(int currentLane, int preferredLane) {
        return track.getSwitchDecision(currentLane, preferredLane);
    }

    public int getPreferredLaneIndex(CarPosition carPosition) {
        return track.getPreferredLaneIndex(carPosition);
    }

    public boolean isOnCurvedPiece(CarPosition carPosition) {
        return track.isOnCurvedPiece(carPosition);
    }
}
