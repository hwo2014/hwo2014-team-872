package com.aronim.hwo2014.message;

import java.util.List;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-15
 * Time: 17h04
 */
public class CarPositionsMessage implements InputMessage {

    private String gameId;

    private int gameTick;

    private List<CarPosition> data;

    public CarPositionsMessage() {
    }

    public String getGameId() {
        return gameId;
    }

    public int getGameTick() {
        return gameTick;
    }

    public List<CarPosition> getCarPositions() {
        return data;
    }

    @Override
    public MessageType getMessageType() {
        return MessageType.CARPOSITIONS;
    }

    public int getNumberOfCars() {
        return data.size();
    }
}
