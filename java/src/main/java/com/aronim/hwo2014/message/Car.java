package com.aronim.hwo2014.message;

import java.io.Serializable;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-15
 * Time: 17h48
 */
public final class Car implements Serializable {

    private CarIdentifier id;

    private Dimensions dimensions;

    public Car() {
    }

    public CarIdentifier getId() {
        return id;
    }

    public Dimensions getDimensions() {
        return dimensions;
    }
}
