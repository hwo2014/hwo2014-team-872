package com.aronim.hwo2014.message;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-15
 * Time: 22h12
 */
public class RaceTest {

    private Gson gson;

    @Before
    public void setUp() throws Exception {

        gson = new GsonBuilder()
                .registerTypeAdapter(InputMessage.class, new InputMessageDeserializer())
                .create();
    }

    @Test
    public void testInitialise() throws Exception {

        InputStream resourceAsStream = getClass().getResourceAsStream("/gameInit.json");
        InputStreamReader inputStreamReader = new InputStreamReader(resourceAsStream);
        GameInitialisedMessage gameInitialisedMessage = (GameInitialisedMessage) gson.fromJson(inputStreamReader, InputMessage.class);

        Race race = gameInitialisedMessage.getRace();

        race.initialise();

        System.out.println();
    }
}
