package com.aronim.hwo2014;

import com.aronim.hwo2014.message.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * User: Kevin W. Sewell
 * Date: 2014-04-15
 * Time: 17h14
 */
public class InputMessageDeserializerTest {

    private Gson gson;

    @Before
    public void setUp() throws Exception {

        gson = new GsonBuilder()
                .registerTypeAdapter(InputMessage.class, new InputMessageDeserializer())
                .create();
    }

    @Test
    public void testDeserializeCarPositionsMessage() throws Exception {

        InputStream resourceAsStream = getClass().getResourceAsStream("/carPositions.json");
        InputStreamReader inputStreamReader = new InputStreamReader(resourceAsStream);
        InputMessage inputMessage = gson.fromJson(inputStreamReader, InputMessage.class);

        Assert.assertNotNull(inputMessage);
        Assert.assertTrue(inputMessage instanceof CarPositionsMessage);
        Assert.assertEquals(1, ((CarPositionsMessage) inputMessage).getNumberOfCars());

    }

    @Test
    public void testDeserializeYourCar() throws Exception {

        InputStream resourceAsStream = getClass().getResourceAsStream("/yourCar.json");
        InputStreamReader inputStreamReader = new InputStreamReader(resourceAsStream);
        InputMessage inputMessage = gson.fromJson(inputStreamReader, InputMessage.class);

        Assert.assertNotNull(inputMessage);
        Assert.assertTrue(inputMessage instanceof YourCarMessage);
        Assert.assertNotNull(((YourCarMessage) inputMessage).getId());

    }

    @Test
    public void testDeserializeGameInitialisedMessage() throws Exception {

        InputStream resourceAsStream = getClass().getResourceAsStream("/gameInit.json");
        InputStreamReader inputStreamReader = new InputStreamReader(resourceAsStream);
        InputMessage inputMessage = gson.fromJson(inputStreamReader, InputMessage.class);

        Assert.assertNotNull(inputMessage);
        Assert.assertTrue(inputMessage instanceof GameInitialisedMessage);
        Assert.assertNotNull(((GameInitialisedMessage) inputMessage).getRace());

    }
}
